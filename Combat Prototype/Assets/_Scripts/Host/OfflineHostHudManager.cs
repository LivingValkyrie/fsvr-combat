﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class OfflineHostHudManager : MonoBehaviour {

    //[Tooltip("Objects to turn on for host")] public GameObject[] thingsToTurnOn;
    //[Tooltip("Objects to turn off for host")] public GameObject[] thingsToTurnOff;

    //[Tooltip("if true then ignores testing as server and sets the server ip to local ip")]
    //public bool testingAsPlayer = false;
    //public bool testingAsServer = false;

    //private void OnEnable() {

    //    if (testingAsPlayer) {
    //        NetworkManager.singleton.serverBindAddress = NetworkHelper.GetLocalIPAddress();
    //        return;
    //    }

    //    if (NetworkManager.singleton.serverBindAddress.Equals(NetworkHelper.GetLocalIPAddress()) || testingAsServer) {
    //        foreach (GameObject obj in thingsToTurnOn) {
    //            obj.SetActive(true);
    //        }
    //        foreach (GameObject obj in thingsToTurnOff) {
    //            obj.SetActive(false);
    //        }
    //    }
    //}

    public void _StartGame() {
        NetworkManager.singleton.StartHost();
    }

}
