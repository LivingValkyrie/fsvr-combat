﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHud : MonoBehaviour {

	public static PlayerHud instance;
	//public static PlayerHud Instance {
	//	get {
	//		if (instance == null) {
	//			var g = new GameObject("PlayerHud");
	//			instance = g.AddComponent<PlayerHud>();
	//			return instance;
	//		} else {
	//			return instance;
	//		}
	//	}
	//}

	public Text subtitleText;
	public Image damageFlash;
	public int damageFlashTransparency = 125;
	public int changeRate = 15;
	public int flashTimes = 1;

	public void UpdateSubtitles(string subtitle, bool selfClear = false) {
		CancelInvoke("ClearSubtitles");
		//print( "should be updating subtitles to " + subtitle );
		subtitleText.text = subtitle;
		if (selfClear) {
			Invoke("ClearSubtitles", 10f);
		}
	}

	public void ClearSubtitles() {
		subtitleText.text = "";
	}

	public void PlayerHit() {
		flashTimes++;
		StartCoroutine("FlashHitFeedback");
	}

	IEnumerator FlashHitFeedback() {
		for (int x = 0; x < flashTimes; x++) {
			//fade in
			for (int i = 0; i < damageFlashTransparency; i += changeRate) {
				//print(i / 255+ " is i in fade in");

				damageFlash.color = new Color(damageFlash.color.r, damageFlash.color.g, damageFlash.color.b, (float)i / 255);
				yield return new WaitForEndOfFrame();
			}

			//fade out
			for (int i = damageFlashTransparency; i >= 0; i -= changeRate) {
				print(i / 255 + " is i in fade out");

				damageFlash.color = new Color(damageFlash.color.r, damageFlash.color.g, damageFlash.color.b, (float)i / 255);
				yield return new WaitForEndOfFrame();
			}
		}

		flashTimes = 0;
	}

	private void Start() {
		if (GetComponentInParent<FSVRPlayer>()) {
			if (!GetComponentInParent<FSVRPlayer>().isLocalPlayer) {
				return;
			}
		}

		if (instance == null) {
			instance = this;
		}
		// else {
		//	Destroy(gameObject);
		//}
	}
}
