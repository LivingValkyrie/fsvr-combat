﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class SCProjectile : NetworkBehaviour {

	public int damage;
	public GameObject impactBurst;

	//public LayerMask ignoreMask;
	//public GameObject deathParticles;
	//public GameObject particles;
	public float particleKillTimer = 2f;
	[SyncVar]
    public GameObject playerWhoFired = null;
	public bool isCannonball = false;
    public float speed = 10;
	//Rigidbody rb;

	// Use this for initialization//////////print(transform.position);
	void Awake() {
		//////////print("awake. s: " + isServer + " lp:" + isLocalPlayer + " c:" + isClient  );

		//rb = GetComponent<Rigidbody>();

        //Physics.IgnoreLayerCollision(0, 10);
        if (!isServer) {
            return;
        }

		////////print(name + " spawned");

		previousPos = transform.position;

		//////////print("invoking");
		Invoke("KillProjectile", 5);
    }

	//private void OnTriggerEnter(Collider other) {
	//    if (!isServer) {
	//        return;
	//    }


	//    if (other.gameObject.tag == "Weapon" || other.gameObject.tag == "Cannon" || other.gameObject.tag == "WeaponPickup") {
	//        return;
	//    }

	//    KillProjectile();

	//}

	private void FixedUpdate() {
        //	RaycastHit hit;
        //	bool test = Physics.SphereCast(transform.position, GetComponent<SphereCollider>().radius, rb.velocity, out hit, rb.velocity.magnitude);

        //	if (test) {
        //		////////print("bullet hit " + hit.transform.name);
        //		Debug.Break();
        //	}

        //need to do raycast between current and previous position

        transform.position += transform.forward * speed * Time.deltaTime;

       	if (!isServer) {
			return;
		}

		Vector3 dir = previousPos - transform.position;
		RaycastHit[] hits =
			Physics.RaycastAll(transform.position, dir, Vector3.Distance(transform.position, previousPos));

		if (hits.Length > 0) {
            foreach (var hit in hits) {
                //if (hit.transform.GetComponent<Enemy>()) {
                //	////////print("hit enemy using raycast");

                //	hit.transform.GetComponent<Enemy>().HitBySCProjectile(gameObject);
                if (hit.transform.GetComponent<Crystal>()) {
                    //	////////print("hit crystal using raycast");

                    hit.transform.GetComponent<Crystal>().HitBySCProjectile(gameObject);
                    //} else if (hit.transform.GetComponent<ImpactProjectile>()) {
                    ////////print("hit impact projectile using raycast");

                    //	hit.transform.GetComponent<ImpactProjectile>().HitBySCProjectile(gameObject);
                    //}
                }
            }
        }

		previousPos = transform.position;

	}

	Vector3 previousPos;


	private void OnCollisionEnter(Collision other) {
        if (!isServer) {
            return;
        }

        if (other.gameObject.tag == "Weapon" || other.gameObject.tag == "Cannon" || other.gameObject.tag == "WeaponPickup") {
            return;
        }

		if (!isCannonball && other.gameObject.tag == "EnemyShip") {
			return;
		}

        if (other.collider.GetComponent<EnemyAlien>()) {
            other.collider.GetComponent<EnemyAlien>().KillMe(playerWhoFired);
        }

        ////////print("collision enter on " + name + " with " + other.collider.name);
        KillProjectile();
    }


    public void KillProjectile() {
        //////////print("kill called");
        if (isServer) {
			var g = Instantiate(impactBurst,transform.position, Quaternion.identity);
			NetworkServer.Spawn(g);

            NetworkServer.Destroy(gameObject);
        }
	}

    [ClientRpc]
    public void RpcDestroy() {
        //////////print("rpc destroy called");
        Destroy(gameObject);
    }
}