﻿using UnityEngine;
using UnityEngine.Networking;
using HTC.UnityPlugin.Vive;
using System.Collections;
using UnityEngine.UI;
using System;
using Sirenix.OdinInspector;
using RootMotion.Demos;
using UnityEngine.SceneManagement;

public class FSVRPlayer : NetworkBehaviour {

    public GameObject[] objectsToAddToDict;
    public MonoBehaviour[] componetsToDisable;
    public GameObject[] objectsToDisable;

    public SteamVR_TrackedObject leftHand, rightHand;

    public GameObject floatingScore;

    public Camera hostCamView, hostCamViewDepth, hostCamViewHud;

    public VRIKCalibrationController vrik;

    public Text nameText;

    [SyncVar(hook = "OnPlayerNameChange")] public string playerName;

    void OnPlayerNameChange(string s) {
        print(name + " has their name updated to " + s);
        nameText.text = s;
        playerName = s;

        if (isLocalPlayer) {
            nameText.gameObject.SetActive(false);
        }
    }

    // Use this for initialization
    void Start() {
        //print("fsvr player start. isLocalPlayer? " + isLocalPlayer);
        if (isLocalPlayer) {
            print("should be setting screen to black");
            SteamVR_Fade.Start(Color.black, 0);
            if (!isServer) {
                GetComponent<Player>().TurnOffColliders();
            }
            SetTrackerIDs();
            //GetComponentInChildren<SteamVR_PlayArea>().BuildMesh();
            //print("post tracker set");
            var spawn = FindObjectOfType<InitialPlaybounds>();
            transform.position = spawn.transform.position;
            transform.rotation = spawn.transform.rotation;

        } else {
            foreach (var com in componetsToDisable) {
                com.enabled = false;
            }

            foreach (var obj in objectsToDisable) {
                obj.SetActive(false);
            }

        }

        if (isServer) {
            //print("should be adding " + gameObject.name + " to host list");
            //GameObject.FindObjectOfType<Host>().AddPlayerToHostList(gameObject);
            VariableHolder.instance.AddPlayerToScoreList(gameObject);

            //RpcNameChange();

        } else {
            GetComponent<Player>().TurnOffColliders();
        }
        
        //foreach ( GameObject obj in objectsToAddToDict ) {
        //	ExitLobbyPlayerTrigger.playerDict.Add( obj, false );
        //}

        if (NumberOfPlayerHolder.instance.numberOfPlayers == VariableHolder.instance.players.Count) {
            print("player count true");
            //if (true) { 
            GetComponent<VRIKCalibrateOnStart>().CalibratePlayer();


            var iks = FindObjectsOfType<VRIKCalibrateOnStart>();
            ////print(iks.Length);
            foreach (var item in iks) {
                ////print(item.calibrated + " calibrated " + name);
                item.CalibratePlayer();
            }

            var players = FindObjectsOfType<FSVRPlayer>();
            print(players.Length + " is fsvrplayer count");
            foreach (var item in players) {
                ////print(item.calibrated + " calibrated " + name);
                item.TellPlayerToUpdateName();
            }

            //if (FindObjectOfType<Captain>()) {
            //	//print("looking for captian");
            //	FindObjectOfType<CaptainDialogueLobby>().enabled = true;
            //	FindObjectOfType<Captain>().Init();

            //}
            //Debug.Break();
            StartCoroutine("FadeIn");

            //if (isServer) {
                GameManager.instance.StartGame();
            //}

        } else {
            Debug.LogWarning("num, players doesnt match");

            print(NumberOfPlayerHolder.instance.numberOfPlayers + " num players");
            print(VariableHolder.instance.players.Count + " holder players");


        }

    }

    void TellPlayerToUpdateName() {
        if (isLocalPlayer) {
            Debug.Log("is local player with ip " + Network.player.ipAddress);
            CmdGetName(Network.player.ipAddress);
        }
    }

    [ClientRpc]
    void RpcNameChange(string s) {
        print("rpc name change received with name of " + s + " and my gameobj name is " + name);
        nameText.text = s;
        playerName = s;

        if (isLocalPlayer) {
            nameText.gameObject.SetActive(false);
        }
    }

    [Command]
    void CmdGetName(string ip) {
        Debug.Log("server cmd received ip of " + ip);

        playerName = VariableHolder.instance.GetNameFromIp(ip);
        RpcNameChange(playerName);
    }


    //private void OnEnable() {
    //    SceneManager.sceneLoaded += OnSceneLoaded;
    //}

    //private void OnDisable() {
    //    SceneManager.sceneLoaded -= OnSceneLoaded;
    //}

    //void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
    //    if (!isLocalPlayer) {
    //        return;
    //    }

    //    //if (scene.buildIndex == 2 || scene.buildIndex == 3) {
    //    //    SteamVR_Fade.Start(Color.clear, 1f);
    //    //}
    //}

    //void OnLevelWasLoaded(int level) {
    //	if (isLocalPlayer) {
    //		if (level == 2) {
    //			SteamVR_Fade.Start(Color.clear, 1f);

    //			//clear anim
    //			//ResetAnims();
    //		}
    //	}
    //}

    public void ResetAnims() {
        foreach (var item in GetComponent<NetworkAnimator>().animator.parameters) {
            if (item.type == AnimatorControllerParameterType.Bool) {
                GetComponent<NetworkAnimator>().animator.SetBool(item.name, false);
            }
        }
    }

    private void SetPositionToZero() {
        transform.position = Vector3.zero;
    }

    IEnumerator FadeIn() {
        print("FADE IN");
        yield return new WaitForSecondsRealtime(1f);
        SteamVR_Fade.Start(Color.clear, 1);
        if (FindObjectOfType<LobbyAudioPlayer>()) {
            FindObjectOfType<LobbyAudioPlayer>().PlayNewClip();
        }
        //transform.Find("You Can Move").gameObject.SetActive(true);
    }

    void SetTrackerIDs() {
        //print("set tracker ids called. isServer? " + isServer);
        try {
            //leftFoot.index = TrackerIds.leftFootId;
            //rightFoot.index = TrackerIds.rightFootId;
            //hip.index = TrackerIds.hipId;
            leftHand.index = (SteamVR_TrackedObject.EIndex)Controller.LeftController.index == SteamVR_TrackedObject.EIndex.None ? 0 : (SteamVR_TrackedObject.EIndex)Controller.LeftController.index;
            rightHand.index = (SteamVR_TrackedObject.EIndex)Controller.RightController.index == SteamVR_TrackedObject.EIndex.None ? 0 : (SteamVR_TrackedObject.EIndex)Controller.RightController.index;
        } catch (Exception e) {
            Debug.LogError("trackers had a null ref, " + e.Message);
        }
    }

    public void SetCameraSettings(RenderTexture mirror) {
        hostCamView.targetTexture = mirror;
        hostCamViewDepth.targetTexture = mirror;
        hostCamViewHud.targetTexture = mirror;

        EnableCamera();

    }

    public void EnableCamera() {
        hostCamView.enabled = true;

        hostCamViewDepth.enabled = true;
        hostCamViewDepth.gameObject.SetActive(true);

        hostCamViewHud.enabled = true;
        hostCamViewHud.gameObject.SetActive(true);
    }

    [ClientRpc]
    public void RpcReCalibrate(GameObject playerToCalibrate) {
        if (isServer || !isLocalPlayer) {
            return;
        }

        playerToCalibrate.GetComponent<FSVRPlayer>().vrik.Calibrate();

    }


    public void MirrorView() {
        //print("mirror view called");
        EnableCamera();
        hostCamView.targetDisplay = 1;
        hostCamView.targetTexture = null;

        hostCamViewDepth.targetDisplay = 1;
        hostCamViewDepth.targetTexture = null;

        hostCamViewHud.targetDisplay = 1;
        hostCamViewHud.targetTexture = null;
    }

    public void DisableMirrorView(RenderTexture texture) {
        //print("disable mirror view called on " + name);
        EnableCamera();
        hostCamView.targetDisplay = 2;
        hostCamView.targetTexture = texture;

        hostCamViewDepth.targetDisplay = 2;
        hostCamViewDepth.targetTexture = texture;

        hostCamViewHud.targetDisplay = 2;
        hostCamViewHud.targetTexture = texture;

    }

    public void DisableCamera() {
        hostCamView.enabled = false;

        hostCamViewDepth.enabled = false;
        hostCamViewDepth.gameObject.SetActive(false);

        hostCamViewHud.enabled = false;
        hostCamViewHud.gameObject.SetActive(false);
    }

    public VariableHolder.PlayerScore.ScoreType type;

    [Button]
    public void GivePoints() {
        //print("give points");
        if (!isServer) {
            //print( "returning not server" );

            return;
        }
        VariableHolder.instance.IncreasePlayerScore(transform.root.gameObject, type, transform.position);
    }

    public void SpawnPointDisplay(Vector3 spawnPos, int value, GameObject player) {
        if (!isServer) {
            ////print("not server");
            return;
        }

        RpcSpawnPointsOnLocalPlayer(spawnPos, value, player);
    }

    [ClientRpc]
    private void RpcSpawnPointsOnLocalPlayer(Vector3 spawnPos, int value, GameObject player) {
        if (isLocalPlayer && player == transform.root.gameObject) {
            ////print("local player and same player that scored");

            var g = Instantiate(floatingScore, spawnPos, Quaternion.identity);
            g.GetComponentInChildren<Text>().text = "+" + value;
        } else {
            ////print("local player? " + isLocalPlayer + " , player is " + player.name + " , root is " + transform.root.name);
        }
    }
}
