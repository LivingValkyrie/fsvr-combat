﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Sirenix.OdinInspector;
using Valve.VR;

//[RequireComponent(typeof(LoadSceneOnStart))]
public class ConnectWithPress : MonoBehaviour {

    //public ViveRoleSetter left, right;

    //public TrackerIdSetter[] setters;

    public bool isHost = false;
	public GameObject standStill;
    public List<GameObject> hostStuff;
	//public GameObject vrLoadLevel;

    // Use this for initialization
    public void EnableInput() {
        Invoke("CanInputIsTrue", 3f);
    }

    void CanInputIsTrue() {
        canInput = true;
		
    }

	public FSVR_AssignController[] trackers;

	private void Start() {
        if (!UnityEngine.XR.XRSettings.enabled) {
            gameObject.SetActive(false);
            foreach (var item in hostStuff) {
                item.SetActive(true);
            }
            return;
        }

		if (PlayerHud.instance) {
			PlayerHud.instance.UpdateSubtitles("Beware real world obstacles. Listen for instructions from your host.");
		} else {
			Invoke("Start", 1);
		}

		RefreshInputs();
	}

	SteamVR_Events.Action trackedDeviceRoleChangedAction;

	ConnectWithPress() {
		//Debug.Log("constructor called");
		trackedDeviceRoleChangedAction = SteamVR_Events.SystemAction(EVREventType.VREvent_TrackedDeviceRoleChanged, OnTrackedDeviceRoleChanged);

	}

	private void OnEnable() {
		trackedDeviceRoleChangedAction.enabled = true;

	}

	private void OnDisable() {
		trackedDeviceRoleChangedAction.enabled = false;
	}
	private void OnTrackedDeviceRoleChanged(VREvent_t vrEvent) {
		RefreshInputs();
	}

	void RefreshInputs() {
		var system = OpenVR.System;
		if (system != null) {
			Controller.leftIndex = (int)system.GetTrackedDeviceIndexForControllerRole(ETrackedControllerRole.LeftHand);
			Controller.rightIndex = (int)system.GetTrackedDeviceIndexForControllerRole(ETrackedControllerRole.RightHand);

			canInput = true;
			//Debug.LogWarning("assigned controllers to " + Controller.leftIndex + " and " + Controller.rightIndex);

			foreach (var item in trackers) {
				int deviceIndex = (int)item.deviceIndex;
				int left = Controller.leftIndex;
				int right = Controller.rightIndex;

				if (deviceIndex.Equals(left)) {
					item.UpdateCanvas("left");
				} else if (deviceIndex == right) {
					item.UpdateCanvas("right");
				} else {
					item.UpdateCanvas("");
				}
			}
		}
	}

	//void UpdateSubs() {
	//	if (PlayerHud.instance) {
	//		PlayerHud.instance.UpdateSubtitles("Beware real world obstacles. Pull LEFT trigger.");

	//	} else {
	//		Invoke("UpdateSubs", 1);
	//	}
	//}

	void Update() {
        //if (NetworkHelper.GetLocalIPAddress().Equals(NetworkHelper.hostIpAddress)) {
        //    return;
        //}

        if (canInput) {
            //print("input enabled");
            if (Controller.RightController.GetPress(Controller.Trigger)) {
				timer += Time.deltaTime;
            }else if (Controller.RightController.GetPressUp(Controller.Trigger) || Controller.RightController.GetPressDown(Controller.Trigger)) {
				timer = 0;
			}

			if(timer >= timeToHold) {
				//foreach (var item in setters) {
				//	if (item) {
				//		item.SetTrackerId();
				//	} else {
				//		continue;
				//	}
				//}
				//FindObjectOfType<SteamVR_LoadLevel>().Trigger();

				StartCoroutine("FadeAndLoad");
			}
        }
    }

	float timer, timeToHold = 3;

    bool canInput = false;



    //void InitController() {
    //    canInput = true;
    //    Controller.InitControllers(left.viveRole.GetDeviceIndex(), right.viveRole.GetDeviceIndex());

    //}

    [Button("loopholes")]
    void Ha() {
		//FindObjectOfType<SteamVR_LoadLevel>().Trigger();  //("Master_Online_new");

        StartCoroutine("FadeAndLoad");
    }

    IEnumerator FadeAndLoad() {
		canInput = false;
		//SteamVR_Overlay.instance.UpdateOverlay();
		//var compositor = OpenVR.Compositor;
        SteamVR_Fade.Start(Color.black, 1, true);
		//compositor.FadeToColor(1f, 255f, 255f, 255f, 1.0f, false);
		standStill.SetActive(true);
        yield return new WaitForSecondsRealtime(1f);
        //NetworkManager.singleton.networkAddress = NetworkManager.singleton.serverBindAddress;
        if (isHost) {
            NetworkManager.singleton.StartHost();
        } else {
            NetworkManager.singleton.StartClient();
        }

        //yield return new WaitForSecondsRealtime(1f);
    }
}

