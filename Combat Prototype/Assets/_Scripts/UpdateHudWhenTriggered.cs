﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateHudWhenTriggered : MonoBehaviour {

	static bool triggered = false;

	private void OnTriggerEnter(Collider other) {
		if (!triggered) {
			PlayerHud.instance.UpdateSubtitles("When the host tells you to, hold the right trigger while standing in pose.");
			triggered = true;
		}
	}

}
