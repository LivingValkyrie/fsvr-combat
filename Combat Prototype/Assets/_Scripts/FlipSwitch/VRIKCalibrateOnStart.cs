﻿using System.Collections;
using System.Collections.Generic;
using RootMotion.Demos;
using RootMotion.FinalIK;
using UnityEngine;
using UnityEngine.Networking;

public class VRIKCalibrateOnStart : NetworkBehaviour {

	 public VRIKCalibrationController mine;
	//public OVRInput.Controller controller;


	public bool calibrated = false;
	// Use this for initialization
	public void CalibratePlayer() {
		//print("calibrate on start local: " + isLocalPlayer + " for " + name);
		//if ( isLocalPlayer ) {
		if (!calibrated) {
			StartCoroutine( "CalibrateLocally" );
			//print( "should be calibrating" );
		}

		//}
	}
	
	// Update is called once per frame
	IEnumerator Calibrate()
    {
		//yield return new WaitForSecondsRealtime(0.5f);
		calibrated = true;
        yield return new WaitForSeconds(1);
        CmdCalibrate();
    }

    IEnumerator CalibrateLocally()
    {
		calibrated = true;
        yield return new WaitForSecondsRealtime(1);
        mine.Calibrate();
		yield return new WaitForEndOfFrame();
		mine.GetComponent<VRIK>().solver.locomotion.weight = 1;

	}

	[Command]
    void CmdCalibrate()
    {
        RpcCalibrate();
    }

    [ClientRpc]
    void RpcCalibrate()
    {
        mine.Calibrate();
		mine.GetComponent<VRIK>().solver.locomotion.weight = 1;
        //print("Calibrated via rpc");
    }
}