﻿using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Random = UnityEngine.Random;


public class GameManager : NetworkBehaviour {

    public float totalGameTime = 240, scoreBoardTime = 15;
    float gameStartTime;
    public float afterIntroDelayBeforeSpawning = 5;
    public float alienTimer = 5;
    int aliensSpawned = 0;
    public int maxEnemies = 20;

    public Transform[] spawnPoints;
    public GameObject alienPrefab;
    public List<Transform> enemyTargets;
    public Transform breweryDoor;

    [System.Serializable]
    public struct SlideAndTimer {
        public GameObject slide;
        public float timer;
    }

    public SlideAndTimer[] slides;
    public float initialDelay;
    public AudioClip introClip;
    public int audioStartSlide = 1;
    public GameObject scoreBoard;

    internal Transform ReturnTarget() {
        int rng = Random.Range(0, enemyTargets.Count);
        int i = 0;
        while (!enemyTargets[rng].gameObject.activeInHierarchy) {
            rng = Random.Range(0, enemyTargets.Count);
            i++;

            if (i >= 10) {
                return breweryDoor;
            }
        }

        return enemyTargets[rng];
    }

    public static GameManager instance;

    void Awake() {
        if (instance == null) {
            instance = this;
        } else {
            Destroy(gameObject);
        }
    }

    [Button]
    public void StartGame() {
        gameStartTime = Time.timeSinceLevelLoad;
        StartCoroutine("GameIntro");

        if (!isServer) {
            return;
        }

        enemyTargets = new List<Transform>();
        var targets = FindObjectsOfType<EnemyTarget>();
        foreach (var item in targets) {
            enemyTargets.Add(item.transform);
        }

    }

    IEnumerator GameIntro() {
        yield return new WaitForSeconds(initialDelay);

        for (int i = 0; i <= slides.Length; i++) {
            if (i > 0) {
                slides[i - 1].slide.SetActive(false);
            }

            if (i == slides.Length) {
                break;
            }

            if (i == audioStartSlide) {
                GetComponent<AudioSource>().PlayOneShot(introClip);

            }

            slides[i].slide.SetActive(true);
            yield return new WaitForSeconds(slides[i].timer);

        }

        //animate curtains raising/watever

        if (isServer) {
            StartSpawning();
        }
    }

    void StartSpawning() {
        StartCoroutine("SpawnAlien");

    }

    IEnumerator SpawnAlien() {
        yield return new WaitForSeconds(afterIntroDelayBeforeSpawning);

        while (true) {

            if (Time.timeSinceLevelLoad >= gameStartTime + totalGameTime) {
                //game over
                var g = Instantiate(scoreBoard, scoreBoard.transform.position, scoreBoard.transform.rotation);
                NetworkServer.Spawn(g);
                scoreBoard.SetActive(false);

                yield return new WaitForSeconds(scoreBoardTime);

                SteamVR_Fade.Start(Color.black, 1);
            } else {
                print(Time.time + " and end time is " + gameStartTime + totalGameTime);
            }

            if (FindObjectsOfType<EnemyAlien>().Length < maxEnemies) {
                int rng = Random.Range(0, spawnPoints.Length);
                var g = Instantiate(alienPrefab, spawnPoints[rng].position, Quaternion.identity);
                g.GetComponent<EnemyAlien>().spawnPoint = spawnPoints[rng];

                NetworkServer.Spawn(g);
                aliensSpawned++;

                float newTime = Mathf.Max(alienTimer - (aliensSpawned / 5), 0.5f);
                print(newTime);
                yield return new WaitForSeconds(newTime);
            } else {
                yield return new WaitForSeconds(alienTimer);
            }
        }
    }

}
