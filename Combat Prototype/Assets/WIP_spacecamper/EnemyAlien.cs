﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.AI;


public class EnemyAlien : NetworkBehaviour {

    public Transform target;
    NavMeshAgent agent;
    //Animator anim;
    NetworkAnimator anim;
    public Transform spawnPoint;

    public float jumpTimer = 0.5f, idleTimeMin = 0.5f, idleTimeMax = 1.5f, distanceToBeer = 0.5f;

	// Use this for initialization
	void Start () {
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponent<NetworkAnimator>();

        StartCoroutine("IdleThenFindTarget");
	}

    IEnumerator IdleThenFindTarget() {
        yield return new WaitForSeconds(jumpTimer);

        yield return new WaitForSeconds(Random.Range(idleTimeMin, idleTimeMax));
        target = GameManager.instance.ReturnTarget();

        agent.SetDestination(target.position);
        anim.animator.SetBool("IsWalking", true);
    }

    private void Update() {
        if (!isServer) {
            return;
        }

        if (!target) {
            return;
        }

        //if (agent.isStopped) {
        //    anim.animator.SetBool("IsWalking", false);
        //}

        if (Vector3.Distance(transform.position, target.position) <= distanceToBeer) {
            agent.isStopped = true;
            //anim.animator.SetBool("IsWalking", false);
            anim.SetTrigger("Grabbing");
        }
    }

    bool killable = true;

    public void KillMe (GameObject killer) {
        if (!killable) {
            return;
        }

        killable = false;
        anim.SetTrigger("Death");
        VariableHolder.instance.IncreasePlayerScore(killer,VariableHolder.PlayerScore.ScoreType.SkeletonKills, transform.position);
        agent.isStopped = true;
    }

    public void Grab() {
        //turn off target
        target.gameObject.SetActive(false);
        //run away
        agent.SetDestination(spawnPoint.position);
        agent.isStopped = false;
        //anim.animator.SetBool("IsWalking", true);
    }

    void DestroyAfterDeath() {
        if (isServer) {
            Destroy(gameObject);
        }
    }
}
