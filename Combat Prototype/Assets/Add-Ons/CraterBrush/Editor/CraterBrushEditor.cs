/*
---------------------- Unity Crater Brush ----------------------
--
-- Crater Brush for Unity (Version 1.1)
-- Code by Ian Deane, based on Terrain Toolkit code by S�ndor Mold�n.
-- 

Copyright (c) 2010 Ian Deane

Developer hereby grants to Licensee a perpetual, non-exclusive, limited license 
to use the Software as set forth in this Agreement.
Licensee shall not modify, copy, duplicate, reproduce, license or sublicense
the Software, or transfer or convey the Software or any right in the Software
to anyone else without the prior written consent of Developer; provided that 
Licensee may make copies of the Software for backup or archival purposes.:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using UnityEngine;
using UnityEditor;

// -------------------------------------------------------------------------------------------------------- EDITOR

[CustomEditor(typeof(CraterBrush))]
public class CraterBrushEditor : Editor {
	
	private bool keydown = false;
	
	public override void OnInspectorGUI() {
		
		//EditorGUIUtility.LookLikeControls();
		if (Application.isPlaying) {
			//return;
		}
		CraterBrush cb = (CraterBrush) target as CraterBrush;
		if (!cb.gameObject) {
			return;
		}
		Terrain terComponent = (Terrain) cb.GetComponent(typeof(Terrain));
		if (terComponent == null) {
			EditorGUILayout.Separator();
			EditorGUILayout.BeginHorizontal();
			GUILayout.Label("The GameObject that Crater Brush is attached to"/*, "errorText"*/);
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.BeginHorizontal();
			GUILayout.Label("does not have a Terrain component."/*, "errorText"*/);
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.Separator();
			EditorGUILayout.BeginHorizontal();
			GUILayout.Label("Please attach a Terrain component."/*, "errorText"*/);
			EditorGUILayout.EndHorizontal();
			return;
		}
		
		GUIContent[] toolbarOptions = new GUIContent[3];
		toolbarOptions[0] = new GUIContent("Elevate");
		toolbarOptions[1] = new GUIContent("Brush");
		toolbarOptions[2] = new GUIContent("Distribute Random");
		cb.toolModeInt = GUILayout.Toolbar(cb.toolModeInt, toolbarOptions);
		switch (cb.toolModeInt) {			
			case 0:
				EditorGUILayout.Separator();
				ElevateGUI();
				EditorGUILayout.Separator();
				break;
			case 1:
				EditorGUILayout.Separator();
				BrushGUI();
				EditorGUILayout.Separator();
				break;
			case 2:
				EditorGUILayout.Separator();
				DistributeGUI();
				EditorGUILayout.Separator();
				break;
		}
		
		if (GUI.changed) {
			EditorUtility.SetDirty(cb);
		}
	}
	
	public void BrushGUI() {
		CraterBrush terrain = (CraterBrush) target as CraterBrush;
		
		terrain.brushPosition = EditorGUILayout.Vector3Field("crater location",terrain.brushPosition);
		
		Rect buttonRect1 = EditorGUILayout.BeginHorizontal();
		buttonRect1.x = buttonRect1.width / 2 - 100;
		buttonRect1.width = 200;
		buttonRect1.height = 18;
		
		if (GUI.Button(buttonRect1, "Activate/Inactivate Brush")) {
			terrain.toggleBrushOn();
		}
		EditorGUILayout.EndHorizontal();
		EditorGUILayout.BeginHorizontal();		
		if ( terrain.turnBrushOnVar ){
			GUILayout.Label("\n\nbrush: active" /*, "errorText"*/);
		} else {
			GUILayout.Label("\n\nbrush: inactive");	
		}
		
		EditorGUILayout.EndHorizontal();
		
		EditorGUILayout.Separator();
		EditorGUILayout.Separator();

		Rect toggleRect = EditorGUILayout.BeginHorizontal();
		toggleRect.x = 110;
		toggleRect.width = 80;
		toggleRect.height = 20;

		EditorGUILayout.EndHorizontal();
		EditorGUILayout.Separator();
		GUILayout.Label("    HINTS:");
		GUILayout.Label("        1. Hold down the [CTRL][SHIFT] to set the crater point");;
		GUILayout.Label("        2. It may be necessary to wiggle the mouse slightly for the brush to take");				
		EditorGUILayout.Separator();
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.PrefixLabel("Brush size");
		terrain.brushSize = EditorGUILayout.Slider(terrain.brushSize, 1, 600);
		EditorGUILayout.EndHorizontal();
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.PrefixLabel("Opacity");
		terrain.brushOpacity = EditorGUILayout.Slider(terrain.brushOpacity, 0, 1);
		EditorGUILayout.EndHorizontal();	
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.PrefixLabel("crater depth");
		terrain.depth_multiplier = EditorGUILayout.Slider(terrain.depth_multiplier, 0, 1);
		EditorGUILayout.EndHorizontal();
	}
	
	//This is the concrete real world function
	public void updateGeneratorProgress(string titleString, string displayString, float percentComplete) {
		EditorUtility.DisplayProgressBar(titleString, displayString, percentComplete);
	}
	
	public void DistributeGUI() {
		CraterBrush terrain = (CraterBrush) target as CraterBrush;
		Terrain ter = (Terrain) terrain.GetComponent(typeof(Terrain));
		if (ter == null) {
			return;
		}
		TerrainData terData = ter.terrainData;
		
		Rect buttonRect1 = EditorGUILayout.BeginHorizontal();
		buttonRect1.x = buttonRect1.width / 2 - 100;
		buttonRect1.width = 200;
		buttonRect1.height = 18;

		if (GUI.Button(buttonRect1, "Distribute Random Craters")) {
			//obsolete but but works better with terrains than RecordObject
			//Undo.RecordObject(terData, "Crater Brush");
			Undo.RegisterCompleteObjectUndo(terData, "Crater Brush");
			CraterBrush.GeneratorProgressDelegate generatorProgressDelegate = new CraterBrush.GeneratorProgressDelegate(updateGeneratorProgress);
			terrain.distributeRandomCraters(generatorProgressDelegate);
			EditorUtility.ClearProgressBar();
			GUIUtility.ExitGUI();			
		}

		EditorGUILayout.EndHorizontal();		
		EditorGUILayout.BeginHorizontal();
		GUILayout.Label("\n\n");
		EditorGUILayout.EndHorizontal();
		EditorGUILayout.Separator();
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.PrefixLabel("num craters");
		terrain.num_craters = (int) EditorGUILayout.Slider(terrain.num_craters, 1, 100);
		EditorGUILayout.EndHorizontal();
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.PrefixLabel("crater depth");
		terrain.depth_multiplier = EditorGUILayout.Slider(terrain.depth_multiplier, 0, 1);
		EditorGUILayout.EndHorizontal();
		
	}

	public void ElevateGUI() {
		CraterBrush terrain = (CraterBrush) target as CraterBrush;
		Terrain ter = (Terrain) terrain.GetComponent(typeof(Terrain));
		if (ter == null) {
			return;
		}
		TerrainData terData = ter.terrainData;
		
		Rect buttonRect1 = EditorGUILayout.BeginHorizontal();
		buttonRect1.x = buttonRect1.width / 2 - 100;
		buttonRect1.width = 200;
		buttonRect1.height = 18;


		EditorGUILayout.EndHorizontal();	
		EditorGUILayout.Separator();
		GUILayout.Label("Terrain points have a minimum and maximum possible height. ");
		GUILayout.Label("Crater cannot be pushed below the minimum height and");
		GUILayout.Label("end up with flat bottoms. Use the slider and button below");
		GUILayout.Label(" to elevate all points on the terrain evenly so");
		GUILayout.Label(" there is space to create deep craters.");
		EditorGUILayout.Separator();
		if (GUILayout.Button("Elevate Terrain")) {
			//obsolete but but works better with terrains than RecordObject
			Undo.RegisterCompleteObjectUndo(terData, "Crater Brush");
			terrain.elevateTerrain();
		}
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.PrefixLabel("elevate by");
		terrain.elevate_by = EditorGUILayout.Slider(terrain.elevate_by, 1, 100);
		EditorGUILayout.EndHorizontal();

	}
	
	private void keyDown1(){
		if (Event.current.shift && Event.current.control) {
			CraterBrush terrain = (CraterBrush) target as CraterBrush;
			Terrain ter = (Terrain) terrain.GetComponent(typeof(Terrain));
			if (ter == null) {
				return;
			}
			TerrainData terData = ter.terrainData;
			//obsolete but but works better with terrains than RecordObject
			Undo.RegisterCompleteObjectUndo(terData, "Crater Brush");
			terrain.craterBrush();
		}	
	}
	
	//not very reliable avoid useing
	private void keyUp(){
	
	}
	
	public void OnSceneGUI() {
		CraterBrush terrain = (CraterBrush) target as CraterBrush;
		
		
		if (terrain.turnBrushOnVar){
			terrain.isBrushHidden = false;
			// The editor does not get key events only mouse events.
			// Most mouse events "click", "other buttons", "shift click" etc... are spoken for
			// Simulate a key event by looking at modifyer keys when receive a mouse event
			if (Event.current.isMouse){
				if (Event.current.control && Event.current.shift){
					if (this.keydown == false){
						this.keyDown1();
					}
					this.keydown = true;		
				} else {
					if (this.keydown == true){
						this.keyUp();
					}
					this.keydown = false;
				}
			}
			
			if(Event.current.isMouse) {
//				if (Event.current.type == EventType.MouseDown){
//					Debug.Log("Mousedown");
//				} else if (Event.current.type == EventType.MouseUp){
//					Debug.Log("Mouseup");	
//				}
				Vector2 mouse = Event.current.mousePosition;
				//mouse.y = Camera.current.pixelHeight - mouse.y + 20;
				Ray ray = HandleUtility.GUIPointToWorldRay(mouse);
				RaycastHit hit;
				if (Physics.Raycast(ray, out hit, Mathf.Infinity)) {
					if (hit.transform.GetComponent("CraterBrush")) {
						terrain.brushPosition = hit.point;						
					}
				}

				terrain.brushPosition = hit.point;				
			}
		}
	}
}
