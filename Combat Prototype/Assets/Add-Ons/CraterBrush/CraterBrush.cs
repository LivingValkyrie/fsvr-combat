/*
---------------------- Unity Ramp Brush ----------------------
--
-- Ramp Brush for Unity (Version 1.1)
-- Code by Ian Deane, based on Terrain Toolkit code by S�ndor Mold�n.
-- 

Copyright (c) 2010 Ian Deane

Developer hereby grants to Licensee a perpetual, non-exclusive, limited license 
to use the Software as set forth in this Agreement.
Licensee shall not modify, copy, duplicate, reproduce, license or sublicense
the Software, or transfer or convey the Software or any right in the Software
to anyone else without the prior written consent of Developer; provided that 
Licensee may make copies of the Software for backup or archival purposes.:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using UnityEngine;
using System;

[ExecuteInEditMode()]
[AddComponentMenu("Terrain/Terrain Crater Brush")]

public class CraterBrush : MonoBehaviour {	
	
	public delegate void GeneratorProgressDelegate(string titleString, string displayString, float percentComplete);
	
	public bool brushOn = false;
	public bool turnBrushOnVar = false;
	public int toolModeInt = 0;
	
	public bool isBrushHidden = false;
	public Vector3 brushPosition;
	public float brushSize = 50.0f;
	public float brushOpacity = 1.0f;
	public float brushSampleDensity = 4.0f;
	public int num_craters = 50;
	public float depth_multiplier = 0.55f;
	public float elevate_by = 50.0f;
	public float max_crater_size = 600;
	
	public void OnDrawGizmos() {
		if (turnBrushOnVar){
			Terrain ter = (Terrain) GetComponent(typeof(Terrain));
			if (ter == null) {
				return;
			}
			// Brush gizmos...

			Gizmos.color = Color.blue;

			float crossHairSize = brushSize / 4.0f;
			Gizmos.DrawLine((brushPosition + new Vector3(-crossHairSize, 0, 0)), (brushPosition + new Vector3(crossHairSize, 0, 0)));
			Gizmos.DrawLine(brushPosition + new Vector3(0, -crossHairSize, 0), brushPosition + new Vector3(0, crossHairSize, 0));
			Gizmos.DrawLine(brushPosition + new Vector3(0, 0, -crossHairSize), brushPosition + new Vector3(0, 0, crossHairSize));
			Gizmos.DrawWireCube(brushPosition, new Vector3(brushSize, 0, brushSize));
			Gizmos.DrawWireSphere(brushPosition, brushSize / 2);		
		}
	}
	
	
	public int[] terrainCordsToBitmap(TerrainData terData, Vector3 v){
			float Tw = (float) terData.heightmapWidth;
			float Th = (float) terData.heightmapHeight;
			Vector3 Ts = terData.size;
			int ny = (int) Mathf.Floor((Tw / Ts.x) * v.x);
		    int nx = (int) Mathf.Floor((Th / Ts.z) * v.z);
			int[] o;
			o = new int[2];
			o[0] = nx;
			o[1] = ny;
			return o;
	}
	
	public float[] bitmapCordsToTerrain(TerrainData terData, int x, int y){
			int Tw = terData.heightmapWidth;
			int Th = terData.heightmapHeight;
			Vector3 Ts = terData.size;
			float ny = (x)* (Ts.x / Th) ;
		    float nx = (y) *(Ts.z / Tw) ;
			float[] o;
			o = new float[2];
			o[0] = nx;
			o[1] = ny;
			return o;
	}	
	
	public void toggleBrushOn(){
		if (turnBrushOnVar){
			turnBrushOnVar = false;
		} else {
			turnBrushOnVar = true;
		}
	}

	public void elevateTerrain(){
		Terrain ter = (Terrain) GetComponent(typeof(Terrain));
		if (ter == null) {
			return;
		}		
		TerrainData terData = ter.terrainData;
		int Tw = terData.heightmapWidth;
		int Th = terData.heightmapHeight;
//		int meshsize = Tw - 1;
		//Vector3 Ts = terData.size;
		
		float[,] heightMapAll = terData.GetHeights(0, 0, Tw, Th);
		for (int i = 0; i < Tw; i++){
			for (int j = 0; j < Th; j++){
				heightMapAll[i,j] += elevate_by / 100.0f;
			}			
		}
		terData.SetHeights(0,0,heightMapAll);
	}
	
	public void distributeRandomCraters(GeneratorProgressDelegate progFunc){
		int numCraters = num_craters;
		Terrain ter = (Terrain) GetComponent(typeof(Terrain));
		if (ter == null) {
			return;
		}		
		TerrainData terData = ter.terrainData;
		int Tw = terData.heightmapWidth;
		int Th = terData.heightmapHeight;
		int meshsize = Tw; 
//		Vector3 Ts = terData.size;
		
		float[,] heightMapAll = terData.GetHeights(0, 0, Tw, Th);
		
		int i = 0; 
	    while (i < numCraters) {
		
			int[] pv = new int[2];
			pv[0] = (int) (UnityEngine.Random.value * meshsize);
			pv[1] = (int) (UnityEngine.Random.value * meshsize);
		
			float opacity = UnityEngine.Random.value;
			
			//power law
			double d;
			double b = 10.0;
			d = .15/(System.Math.Pow(UnityEngine.Random.value + 1.0/b, 3.0) * System.Math.Pow(b, 3.0));
			
			//gaussian distribution
			//double x = UnityEngine.Random.value;
			//d = .15/System.Math.Exp(x*x/.32);
				         
			int crater_radius = (int) (3.0 + (d * meshsize));
			
			if (crater_radius < max_crater_size){
				makeCraterOnHeightField(heightMapAll, pv, crater_radius*2, crater_radius*2, opacity, meshsize, terData);
				terData.SetHeights(0,0,heightMapAll);
			}
			i++;
			progFunc("Distributing craters","Creating " + i + " of " + numCraters, ((float)i) / ((float)numCraters));
		}
	}
	
	public void craterBrush() {
		Terrain ter = (Terrain) GetComponent(typeof(Terrain));
		if (ter == null) {
			return;
		}				
		try { 
			TerrainData terData = ter.terrainData;
			int Tw = terData.heightmapWidth;
			int Th = terData.heightmapHeight;
			int meshsize = Tw; 
			Vector3 Ts = terData.size;		
			Vector3 ls = transform.localScale; //need to remember and reset the scale because the InverseTransform does not work properly for terrains because they ignore the scale
			transform.localScale = new Vector3(1.0f,1.0f,1.0f);
			Vector3 localEndPosition = transform.InverseTransformPoint(brushPosition);
			transform.localScale = ls;
			
			int Sx = (int) Mathf.Floor((Tw / Ts.x) * brushSize);
			int Sz = (int) Mathf.Floor((Th / Ts.z) * brushSize);
			
			int[] pf = terrainCordsToBitmap(terData,localEndPosition);
			
			float[,] heightMapAll = terData.GetHeights(0, 0, Tw, Th);
			
			makeCraterOnHeightField(heightMapAll, pf, Sx, Sz, brushOpacity, meshsize, terData);
			
			terData.SetHeights(0,0,heightMapAll);
		} catch (Exception e) {
		 	Debug.LogError("A brush error occurred: "+e);
		}			
	}
	
	void makeCraterOnHeightField(float[,] heightMapAll, int[] pv, int Sx, int Sz, float opacity, int meshsize, TerrainData ter){
		//copy heights to a temporary heightmap
		int Px, Py;
		Px = pv[0] - Sx/2;
		Py = pv[1] - Sz/2;	
		float[,] heightMap = new float[Sx,Sz];
		for (int i = 0; i < Sx; i ++){
			for (int j = 0; j <  Sz; j++){
				if (Px + i >= 0 && Py + j >=0 && Px + i < meshsize && Py + j < meshsize) {
					heightMap[i,j] = heightMapAll[Px + i,Py + j] ;
				} else {
					heightMap[i,j] = 0;
				}
			}
		}				
		
		doPerfectCrater(heightMap, heightMapAll, Sx, meshsize, pv[0], pv[1]);
	
		// Apply it to the terrain object
		//float sampleRadius = Sx / 2.0f;
		for (int i = 0; i < Sx; i++) {
			for (int j = 0; j < Sz; j++) {
				float newHeightAtPoint, oldHeightAtPoint;
				newHeightAtPoint = heightMap[i, j];
				if (Px + i >= 0 && Py + j >=0 && Px + i < meshsize && Py + j < meshsize) {
					oldHeightAtPoint = heightMapAll[Px + i, Py + j];
				} else {
					oldHeightAtPoint = 0.0f;
				}  
				float weightAtPoint = opacity;
				float blendedHeightAtPoint = (newHeightAtPoint * weightAtPoint) + (oldHeightAtPoint * (1.0f - weightAtPoint));
				heightMap[i, j] = blendedHeightAtPoint;
				
			}
		}
		
		for (int i = 0; i < Sx; i ++){
			for (int j = 0; j <  Sz; j++){
				if (Px + i >= 0 && Py + j >=0 && Px + i < meshsize && Py + j < meshsize) {
					heightMapAll[Px + i,Py + j] = heightMap[i,j];
				}
			}
		}
	}
	
	void doPerfectCrater(float[,] heightMap,float[,]  heightMapAll,int Sx, int meshsize, int Px, int Py){
		//do perfect crater here to temporary heightmap
		int crater_radius = Sx / 2;
 
		double adjRad, y;
		for (int i = -crater_radius; i < crater_radius; i++) {
		    for (int j = -crater_radius; j < crater_radius; j++) {
				y = 0.0;
				//antialiasing average four points around i,j
				for (double k = -.5; k < 1.0;k +=1.0){
					for (double l = -.5; l < 1.0;l +=1.0){
						double id = i + k;
						double jd = j + l;
						/* check if outside */
						adjRad = (double)(id*id+jd*jd)/crater_radius/crater_radius;
						if (adjRad > 1.0) continue;
				
						/* inside the crater area */
						double roundWeight =  1.0;
						double squareWeight = 1.0 - roundWeight;
						y += crater_radius * .03 *(craterShape(adjRad)*roundWeight + squareShape(adjRad)*squareWeight);
					}
				}
				y /= 4.0;
				
			    int i_fullmap = Px + (i); 
				int j_fullmap = Py + (j);
				if (i_fullmap >=0 && i_fullmap < meshsize && j_fullmap >= 0 && j_fullmap < meshsize){				    
					heightMap[i + crater_radius,j + crater_radius] = (float) y + heightMap[i + crater_radius,j + crater_radius]; 
				}
			}
		}
	}

	double craterShape(double adjRad)
	{
		double val;
		double alpha = (35.25/180.0 * System.Math.PI);     
		double crater_depth = .85;
		double b = System.Math.Sin(alpha);
	   	double a = crater_depth - System.Math.Cos(alpha);
	   	double c = 50.0;
	   	double radialcoord;
		
	   radialcoord = System.Math.Sqrt(System.Math.Abs(adjRad));
	
	   if (radialcoord > b) {
	     /* outer region gauss distribution */
	     val = a * System.Math.Exp(-c*(radialcoord-b)*(radialcoord-b));
	   } else {
	     /* inner region sphere segment */
	     val = crater_depth - System.Math.Sqrt(1.0 - adjRad);
	   }
		return ((double)depth_multiplier) * val;
	}
	
	double squareShape(double adjRad){
		double val;
		double alpha = (35.25/180.0 * System.Math.PI);     
		double crater_depth = .85;
		double b = System.Math.Sin(alpha);
	   	//double d = crater_depth - System.Math.Cos(alpha);
	   	//double c = 50.0;
		double m = 1/6.0;
		double yint = - m;
	   	double radialcoord;
		
	   radialcoord = System.Math.Sqrt(System.Math.Abs(adjRad));
	
	   if (radialcoord > b) { //outer
		 val = m * radialcoord + yint; //cone sloping out
	   } else { //inner
	     val = -crater_depth;
	   }
		return ((double)depth_multiplier) * val;		
	}
}