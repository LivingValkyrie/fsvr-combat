﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobbyAudioPlayer : MonoBehaviour {

    public AudioClip moveClip;

	private void Start() {
		PlayerHud.instance.UpdateSubtitles("Please stand still.");
	}

	public void PlayNewClip() {
        GetComponent<AudioSource>().clip = moveClip;
		PlayerHud.instance.UpdateSubtitles("You may now move freely", true);
        GetComponent<AudioSource>().loop = false;
        GetComponent<AudioSource>().Play();
        Destroy(gameObject, moveClip.length + 0.5f);
    }

}
