﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FSVR_AssignController : MonoBehaviour {

    //SteamVR_Controller.Device device;
    public Text canvasText;
	//bool hasTried = false;

	public uint deviceIndex {
		get {
			return (uint)GetComponent<SteamVR_TrackedObject>().index;
		}
	}

	void Start () {
        //device = Controller.GetById((int)GetComponent<SteamVR_TrackedObject>().index);
        //canvasText = GetComponentInChildren<Text>();
        //Invoke("DestroyIfNotRendering", 0.5f);
    }
	
	
	void Update () {
		//if (hasTried) {
		//	return;
		//}

  //      if (device != null){
  //          //print(device + " id is " + device.index);

  //          if (device.GetPressDown(Controller.Trigger)) {
  //              //print("hit trigger on device i:" + device.index);
  //              canvasText.transform.parent.gameObject.SetActive(true);
  //              canvasText.text = Controller.InitControllers(device.index, GetComponent<SteamVR_TrackedObject>().index);

		//		if (canvasText.text == "Left") {
		//			PlayerHud.instance.UpdateSubtitles("Left controller assigned. Pull RIGHT trigger.");

		//		} else if (canvasText.text == "Right") {
		//			PlayerHud.instance.UpdateSubtitles("Right controller assigned. Carefully move to colored foot marker, beware real world obstacles.");
		//		}

		//		hasTried = true;

  //              if (Controller.initialized) {
  //                  FindObjectOfType<ConnectWithPress>().EnableInput();
  //              }
  //          }
  //      }

		if (!string.IsNullOrEmpty(canvasText.text)) {
			canvasText.transform.parent.gameObject.SetActive(true);
		}
	}

	public void UpdateCanvas(string text) {
		//print("update canvas called");
		canvasText.transform.parent.gameObject.SetActive(true);
		canvasText.text = text;
	}

	void DestroyIfNotRendering() {
        if (transform.childCount < 2) {
            Destroy(gameObject);
        }
    }
}
