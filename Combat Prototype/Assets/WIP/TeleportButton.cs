﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class TeleportButton : NetworkBehaviour {
    public float timer = 3;
    public Transform nextSpot;

    private void OnTriggerEnter(Collider other) {
        print("sdefsdgewd");
        if (!isServer) {
            return;
        }

        if (other.transform.root.tag == "Player") {
            print("player found");

            StopAllCoroutines();
            StartCoroutine("Countdown");
        }
    }

    private void OnTriggerExit(Collider other) {
        print("exiting");
        StopAllCoroutines();

    }

    IEnumerator Countdown() {
        print("pre timer");

        yield return new WaitForSeconds(timer);

        print("timer finished");

        RpcTeleportPlayers();

    }

    [ClientRpc]
    void RpcTeleportPlayers() {
        print("post countdown");

        foreach (var p in FindObjectsOfType<Player>()) {
            print("teleporting " + p.name);
            p.transform.root.position = nextSpot.position;
            p.transform.root.rotation = nextSpot.rotation;
            p.playbounds = nextSpot;
        }

        print("done telewporting");
    }

}
