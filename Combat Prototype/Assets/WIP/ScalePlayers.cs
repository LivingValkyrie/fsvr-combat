﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ScalePlayers : NetworkBehaviour {

    public Transform playSpace;
    public bool asScaler = true;

    public void ScaleAllPlayers(float scaleFactor) {
        if (!isServer) {
            return;
        }

        RpcScalePlayers(scaleFactor);
    }

    [ClientRpc]
    void RpcScalePlayers(float scaleFactor) {

        playSpace.localScale = Vector3.one * scaleFactor;

        foreach (var p in FindObjectsOfType<Player>()) {
            Vector3 scale = asScaler ? p.transform.root.localScale : Vector3.one;
            p.transform.root.localScale = scale * scaleFactor;
        }

    }
}
