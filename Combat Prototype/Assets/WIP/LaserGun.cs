﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;


public class LaserGun : NetworkBehaviour {

    public Transform shotPoint;
    public float range = 50f;
    public float timer = 0.15f;
    public GameObject toSpawn;
    public LineRenderer lr;
    public Color laserColor;
    public Player player;

    public GameObject muzzleFlash, projectile, impact;

    private void Start() {
        player = GetComponent<Player>();        
    }

    // Update is called once per frame
    void Update() {
        if (!isLocalPlayer) {
            return;
        }

        if (player.IsDead) {
            return;
        }


        if (Controller.RightController.GetPressDown(Controller.Trigger)) {
            CmdShoot(false);
            //Vector3 rot = Quaternion.identity.eulerAngles;
            ////if (data.spread > 0) {
            ////	var variance = Quaternion.AngleAxis(Random.Range(0, 360), rot) * Vector3.up * Random.Range(0, data.spread);
            ////	rot += variance;
            ////}

            //var bullet = Instantiate(projectile, shotPoint.position, Quaternion.Euler(rot));

            ////bullet.GetComponent<Rigidbody>().AddForce(shotPoint.forward * power, ForceMode.Impulse);
            ////bullet.GetComponent<SCProjectile>().damage = damage;
            //bullet.GetComponent<SCProjectile>().playerWhoFired = transform.root.gameObject;
            //bullet.GetComponent<SCProjectile>().speed = power;
            //bullet.transform.forward = shotPoint.forward;


            //CmdShootSpecific(shotPoint.position, rot, shotPoint.forward * power, transform.root.gameObject);
        } else if (Controller.RightController.GetPress(Controller.Trigger)) {
            CmdShoot(true);
        }
    }

    float lastShot = 0;
    public float timeBetweenShots = 0.3f;

    [Command]
    void CmdShootSpecific(Vector3 pos, Vector3 rot,Vector3 dir, GameObject whoFired) {
        var bullet = Instantiate(projectile, pos, Quaternion.Euler(rot));

        bullet.GetComponent<Rigidbody>().AddForce(dir, ForceMode.Impulse);
        //bullet.GetComponent<SCProjectile>().damage = damage;
        bullet.GetComponent<SCProjectile>().playerWhoFired = whoFired;

        RpcSpawnBulletOnClient(pos, rot, dir, whoFired);
    }

    [ClientRpc]
    void RpcSpawnBulletOnClient(Vector3 pos, Vector3 rot, Vector3 dir, GameObject whoFired) {
        if (transform.root.gameObject == whoFired) {
            return;
        }

        var bullet = Instantiate(projectile, pos, Quaternion.Euler(rot));

        bullet.GetComponent<Rigidbody>().AddForce(dir, ForceMode.Impulse);
        //bullet.GetComponent<SCProjectile>().damage = damage;
        bullet.GetComponent<SCProjectile>().playerWhoFired = whoFired;
        bullet.transform.forward = shotPoint.forward;

        //NetworkServer.Spawn(bullet);
    }

    void Spawn(Vector3 pos) {
        if (!isServer) {
            return;
        }

        var g = Instantiate(toSpawn, pos, Quaternion.identity);
        NetworkServer.Spawn(g);

    }

    [Command]
    void CmdShoot(bool checkTime) {
        if (!isServer) {
            return;
        }

        if (!checkTime) {
            SpawnBullet(false, 0);
            lastShot = Time.timeSinceLevelLoad;
        } else {
            if (Time.timeSinceLevelLoad > lastShot + timeBetweenShots) {
                SpawnBullet(false, 0);
                lastShot = Time.timeSinceLevelLoad;
            }
        }

    }

    private void FireTheLaser(bool spawn, bool teleport) {
        RaycastHit hit;
        lr.positionCount = 2;
        lr.SetPosition(0, shotPoint.position);

        if (Physics.Raycast(shotPoint.position, shotPoint.forward, out hit, range)) {
            if (hit.collider.tag == "Shield") {
                hit.collider.GetComponentInParent<Shield>().Damage();
            } else if (hit.collider.tag == "Player") {
                if (hit.collider.transform.root.gameObject != transform.root.gameObject) {
                    hit.collider.GetComponentInParent<Player>().ChangeHealth(25);
                }
            } else if (hit.collider.tag == "Scalable") {
                hit.collider.GetComponent<ScalableObject>().ChangeScale();
            } else if (hit.collider.GetComponent<ScalePlayerButton>()) {
                hit.collider.GetComponent<ScalePlayerButton>().CallScale();
            }

            if (spawn) {
                Spawn(hit.point);

            }

            if (teleport) {
                FindObjectOfType<GhostFreeRoamCamera>().Teleport(hit.point);
            }

            lr.SetPosition(1, hit.point);
        } else {
            lr.SetPosition(1, shotPoint.position + (shotPoint.forward * range));
        }

        RpcShowLaser(lr.GetPosition(0), lr.GetPosition(1));

        StopCoroutine("FadeLaser");
        StartCoroutine("FadeLaser");
    }

    public GameObject projectileOld;
    public float power = 5;

    public void SpawnBullet(bool isLeft, ushort hapticSize) {
        if (isServer) {
            Vector3 rot = shotPoint.eulerAngles;
            //if (data.spread > 0) {
            //	var variance = Quaternion.AngleAxis(Random.Range(0, 360), rot) * Vector3.up * Random.Range(0, data.spread);
            //	rot += variance;
            //}

            var bullet = Instantiate(projectile, shotPoint.position, Quaternion.Euler(rot));

            bullet.GetComponent<Rigidbody>().AddForce(shotPoint.forward * power, ForceMode.Impulse);
            //bullet.GetComponent<SCProjectile>().damage = damage;
            bullet.GetComponent<SCProjectile>().playerWhoFired = transform.root.gameObject;

            var flash = Instantiate(muzzleFlash, shotPoint.position, Quaternion.Euler(rot));

            NetworkServer.Spawn(bullet);
            NetworkServer.Spawn(flash);

            RpcSetMuzzleLock(flash);
            //RpcEnableFlash();
        }
    }

    [ClientRpc]
    private void RpcSetMuzzleLock(GameObject flash) {
        flash.GetComponent<ObjectPositionLock>().posPoint = shotPoint.gameObject;
    }

    [ClientRpc]
    void RpcEnableFlash() {
        muzzleFlash.SetActive(true);
        Invoke("TurnOffFlash", 0.2f);
    }

    void TurnOffFlash() {
        muzzleFlash.SetActive(false);
    }

    [ClientRpc]
    void RpcShowLaser(Vector3 from, Vector3 to) {
        if (isServer) {
            return;
        }

        lr.positionCount = 2;

        lr.SetPosition(0, from);
        lr.SetPosition(1, to);

        StopCoroutine("FadeLaser");
        StartCoroutine("FadeLaser");
    }

    IEnumerator FadeLaser() {
        //lr.SetPosition(0, shotPoint.position);

        yield return new WaitForSeconds(timer);

        lr.positionCount = 0;


    }
}
