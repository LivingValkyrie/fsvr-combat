﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableIKAfterDelay : MonoBehaviour {

	public float delay = 1.5f;
	public RootMotion.FinalIK.VRIK vrik;
	//public RootMotion.Demos.VRIKCalibrationController con;
	// Use this for initialization
	void Start () {
		Invoke("EnableIK", delay);
	}

	//private void Update() {
	//	if (Controller.RightController.GetPressDown(Controller.Trigger)) {
	//		EnableIK();
	//	}
	//}

	GameObject leftHand, rightHand;
	// Update is called once per frame
	void EnableIK() {
		foreach (var item in GetComponent<ConnectWithPress>().trackers) {
			int deviceIndex = (int)item.deviceIndex;
			int left = Controller.leftIndex;
			int right = Controller.rightIndex;

			if (deviceIndex.Equals(left)) {
				leftHand = item.gameObject;
			} else if (deviceIndex == right) {
				rightHand = item.gameObject;
			}

		}

		vrik.solver.leftArm.target = leftHand.transform;
		vrik.solver.rightArm.target = rightHand.transform;

		//con.leftHandTracker = leftHand.transform;
		//con.rightHandTracker = rightHand.transform;
		//con.Calibrate();

	}
}
