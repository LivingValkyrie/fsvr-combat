﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisablePlayerSoulsOnStart : MonoBehaviour {

	// Use this for initialization
	void Start () {
		foreach (var v in FindObjectsOfType<Player>()) {
			v.TurnOffAllParticles();
		}
	}

}
