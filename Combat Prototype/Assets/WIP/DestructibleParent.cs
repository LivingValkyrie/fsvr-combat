﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class DestructibleParent : NetworkBehaviour {

    public GameObject crystalDeathParticles;
    


    internal void DestroyCrystal(int i) {
        if (isServer) {
            //NetworkServer.Destroy( g );
            ////////print("i am the server, destroy crystal was called, should have spawned fragments");

            GameObject go = Instantiate(crystalDeathParticles, transform.GetChild(i).position, Quaternion.identity);
            NetworkServer.Spawn(go);

            Destroy(transform.GetChild(i).gameObject);
            RpcDestroyCrystalOnClient(i);

        } //else {
          //	////////print("im not the server, but destroy crystal was called, should have spawned fragments");
          //}
    }

    [ClientRpc]
    internal void RpcDestroyCrystalOnClient(int i) {
        if (isServer) {
            return;
        }

        Destroy(transform.GetChild(i).gameObject);

    }
}
