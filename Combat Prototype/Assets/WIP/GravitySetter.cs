﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravitySetter : MonoBehaviour {

    private void OnTriggerEnter(Collider other) {
        other.transform.root.up = transform.up;
    }

    //public float range = 3;

    //// Update is called once per frame
    //void FixedUpdate() {

    //    RaycastHit hit;
    //    if (Physics.Raycast(transform.position, Vector3.down, out hit, range)) {
    //        transform.root.up = hit.normal;
    //    }
    //}


}
