﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;


public class Shield : NetworkBehaviour {

    [SyncVar(hook = "OnHealthChange")]
    public int health = 3;
    public GameObject shieldObj;

    public Color[] hpCols;

    void OnHealthChange(int hp) {
        health = hp;
        if (hp <= hpCols.Length && hp >= 0) {
            shieldObj.GetComponent<Renderer>().material.color = hpCols[health];
        }

    }

    // Use this for initialization
    [ClientRpc]
	public void RpcDie () {
        if (isServer) {
            return;
        }

        shieldObj.GetComponent<Collider>().enabled = false;
        shieldObj.GetComponent<Renderer>().enabled = false;
    }

    public void Damage() {
        if (!isServer) {
            return;
        }
        health--;
        if (health <= 0) {
            shieldObj.GetComponent<Collider>().enabled = false;
            shieldObj.GetComponent<Renderer>().enabled = false;

            RpcDie();
        }
    }

    // Update is called once per frame
    public void Respawn () {
        if (!isServer) {
            return;
        }
        health = 3;
        shieldObj.GetComponent<Collider>().enabled = true;
        shieldObj.GetComponent<Renderer>().enabled = true;

        RpcRespawn();
    }

    [ClientRpc]
    void RpcRespawn() {
        shieldObj.GetComponent<Collider>().enabled = true;
        shieldObj.GetComponent<Renderer>().enabled = true;
    }
}
