﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ScalableObject : NetworkBehaviour {

    public float[] scales;
    [SyncVar(hook = "OnScaleChange")]
    int currentScaleIndex = 0;

    private void Start() {
      
    }

    public void ChangeScale() {
        if (currentScaleIndex + 1 >= scales.Length) {
            currentScaleIndex = 0;
        } else {
            currentScaleIndex++;
        }
    }

    void OnScaleChange(int i) {
        if (i < scales.Length && i >= 0) {
            transform.localScale = Vector3.one * scales[i];
        }
    }


}
