﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ScalePlayers))]
public class ScalePlayerButton : MonoBehaviour {

    ScalePlayers scaler;
    public float scaleFactor = 2;

    private void Start() {
        scaler = GetComponent<ScalePlayers>();        
    }

    public void CallScale() {
        scaler.ScaleAllPlayers(scaleFactor);
    }
}
